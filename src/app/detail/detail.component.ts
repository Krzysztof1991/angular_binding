import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-detail',
  template:
      '<h2>Detail</h2>' +
      '<ul *ngFor="let employee of employees">' +
      '<li>{{employee.id}}, {{employee.name}}, {{employee.age}}</li>' +
      '</ul>',
  styleUrls: []
})
export class DetailComponent implements OnInit {

  public employees = [];

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
   this.employeeService.getEmployees()
       .subscribe(data => this.employees = data,
           err => {
         console.error(err);
           },() => {

           });
  }

}
