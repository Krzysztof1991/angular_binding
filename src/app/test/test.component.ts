import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-test',
  template: '<h2 [class.text-danger]="danger">Welcome {{ name }}</h2>' +
      '<h2 [class]="successClass">{{greetUser()}}</h2>' +
      '{{2+2}}' +
      '<h2 class="text-success">{{url}}</h2>' +
      '<input class ="text-success" [id]="myId" type="text" value="imie">' +
      '<input [disabled] ="isDisabled" type="text" value ="imie">' +
      '<h2 [style.color]="orange">Krzysiek</h2>' +
      '<h2 [ngStyle]="titleStyles">Krzysiek</h2>' +
      '<button (click)="onClick($event)">Przycisk</button> {{greeting}}' +
      '<input #myInput type="text">' +
      '<button (click)="logMessage(myInput.value)">Log</button>' +
      '<input [(ngModel)]="imie" type="text">' +
      '<h2 [ngClass]="messageClasses">CODEEVOLUTION</h2>' +
      '{{imie}}' +
      '<h2>{{parentData}}</h2>' +
      '<h2>{{city}}</h2>' +
      '<button (click)="fireEvent()">Send Event</button>' +
      '<h2> {{name}}</h2>' +
      '<h2>{{name| lowercase }}</h2>' +
      '<h2>{{name| uppercase }}</h2>' +
      '<h2>{{name| titlecase }}</h2>' +
      '<h2>{{name | slice:3:5}}</h2>' +
      '<h2>{{person| json}}</h2>',



  styles: [
  '.text-success {color: green;}' +
  '.text-danger {color:red;']
})
export class TestComponent implements OnInit {

  public imie = '';

  public name = 'Krzysiek';
  public orange = 'orange';
  public isDisabled = 'false';
  public myId = 'testId';
  public successClass = 'text-success';
  public url = window.location.href;
  public danger = true;
  public hasError = false;
  public isSpecial = "true";
  public titleStyles = {
    color: 'red',
    fontStyle: 'italic'
  }
  public greeting = "";

  public person = {
    "firstName":"John",
    "lastName":"Doe"
  }

  public onClick(event){
    console.log('sdsd');
    this.greeting = event.type;
  }
  @Input() public parentData;
  @Input('parentData') public city;
  @Output() public childEvent = new EventEmitter();

  fireEvent(){
    this.childEvent.emit('HEJ HEJ HEJ');
  }

  public messageClasses = {
    "text-success": !this.hasError,
    "text-danger": this.hasError,
  }

  public logMessage(value){
    console.log(value);
  }
  constructor( private _employeeService: EmployeeService) { }

  ngOnInit() {
  }
  greetUser(){
    return "Hello " + this.name;
  }

}
